extern crate rand;
use rand::{thread_rng, Rng};
#[cfg(test)]
mod tests {
    #[test]
    fn count_ones() {
        assert_eq!(64, 3_u64.count_ones() + 3_u64.count_zeros());
    }
}
const BINARY_VECTOR_SIZE: usize = 32;
const U64_2_POW_N: [u64; 64] = [
    9_223_372_036_854_775_808,
    4_611_686_018_427_387_904,
    2_305_843_009_213_693_952,
    1_152_921_504_606_846_976,
    576_460_752_303_423_488,
    288_230_376_151_711_744,
    144_115_188_075_855_872,
    72_057_594_037_927_936,
    36_028_797_018_963_968,
    18_014_398_509_481_984,
    9_007_199_254_740_992,
    4_503_599_627_370_496,
    2_251_799_813_685_248,
    1_125_899_906_842_624,
    562_949_953_421_312,
    281_474_976_710_656,
    140_737_488_355_328,
    70_368_744_177_664,
    35_184_372_088_832,
    17_592_186_044_416,
    8_796_093_022_208,
    4_398_046_511_104,
    2_199_023_255_552,
    1_099_511_627_776,
    549_755_813_888,
    274_877_906_944,
    137_438_953_472,
    68_719_476_736,
    34_359_738_368,
    17_179_869_184,
    8_589_934_592,
    4_294_967_296,
    2_147_483_648,
    1_073_741_824,
    536_870_912,
    268_435_456,
    134_217_728,
    67_108_864,
    33_554_432,
    16_777_216,
    8_388_608,
    4_194_304,
    2_097_152,
    1_048_576,
    524_288,
    262_144,
    131_072,
    65536,
    32768,
    16384,
    8192,
    4096,
    2048,
    1024,
    512,
    256,
    128,
    64,
    32,
    16,
    8,
    4,
    2,
    1,
];

#[derive(Copy, Clone)]
struct BinaryVector {
    data: [u64; BINARY_VECTOR_SIZE],
}

impl BinaryVector {
    pub fn new(u64_list: [u64; BINARY_VECTOR_SIZE]) -> BinaryVector {
        BinaryVector { data: u64_list }
    }
    pub fn from_bits(bit_list: [bool; BINARY_VECTOR_SIZE * 64]) -> BinaryVector {
        let mut data: [u64; BINARY_VECTOR_SIZE] = [0; BINARY_VECTOR_SIZE];
        for i in 0..BINARY_VECTOR_SIZE {
            for x in 0..64 {
                if bit_list[i * 64 + x] {
                    data[i] |= U64_2_POW_N[x];
                }
            }
        }
        BinaryVector { data }
    }
    pub fn dot(&self, other: &BinaryVector) -> bool {
        let mut sum: u32 = 0;
        for i in 0..BINARY_VECTOR_SIZE {
            sum += (self.data[i] ^ other.data[i]).count_ones();
        }
        sum > 32 * (BINARY_VECTOR_SIZE as u32)
    }
    pub fn to_output(&self, output_size: usize) -> Vec<u64> {
        self.data[..output_size]
            .iter()
            .map(|x| u64::from(x.count_ones()))
            .collect::<Vec<u64>>()
    }
}
#[derive(Copy, Clone)]
struct BinaryMatrix {
    data: [BinaryVector; BINARY_VECTOR_SIZE * 64],
}

impl BinaryMatrix {
    pub fn new(matrix_list: [u64; BINARY_VECTOR_SIZE * BINARY_VECTOR_SIZE * 64]) -> BinaryMatrix {
        let mut layer: [BinaryVector; BINARY_VECTOR_SIZE * 64] =
            [BinaryVector::new([0; BINARY_VECTOR_SIZE]); BINARY_VECTOR_SIZE * 64];
        let mut array = [0; BINARY_VECTOR_SIZE];
        for i in 0..BINARY_VECTOR_SIZE * 64 {
            for x in 0..BINARY_VECTOR_SIZE {
                array[x] = matrix_list[i * BINARY_VECTOR_SIZE + x];
            }
            layer[i] = BinaryVector::new(array);
        }
        BinaryMatrix { data: layer }
    }
    pub fn mul(&self, other: &BinaryVector) -> BinaryVector {
        let mut bit_list: [bool; BINARY_VECTOR_SIZE * 64] = [false; BINARY_VECTOR_SIZE * 64];
        for (i, item) in self.data.iter().enumerate() {
            bit_list[i] = item.dot(&other);
        }
        BinaryVector::from_bits(bit_list)
    }
}

pub struct E5099f26 {
    size_of_game_state: usize,
    number_of_moves: usize,
    nn: Vec<BinaryMatrix>,
    rng: rand::ThreadRng,
}

impl E5099f26 {
    pub fn new(size_of_game_state: usize, number_of_moves: usize, layers: usize) -> E5099f26 {
        assert!(size_of_game_state >= 1 && size_of_game_state <= BINARY_VECTOR_SIZE);
        assert!(number_of_moves >= 1 && number_of_moves <= BINARY_VECTOR_SIZE);
        assert!(layers >= 2);
        let mut new: E5099f26 = E5099f26 {
            size_of_game_state,
            number_of_moves,
            nn: vec![BinaryMatrix::new([0; BINARY_VECTOR_SIZE * BINARY_VECTOR_SIZE * 64]); layers],
            rng: thread_rng(),
        };
        new.randomise();
        new
    }
    fn randomise(&mut self) {
        for i in 0..self.nn.len() {
            for x in 0..BINARY_VECTOR_SIZE * 64 {
                for y in 0..BINARY_VECTOR_SIZE {
                    self.nn[i].data[x].data[y] = self.rng.gen();
                }
            }
        }
    }
    pub fn choose(&self, input: &[u64]) -> Vec<u64> {
        let mut u64_list: [u64; BINARY_VECTOR_SIZE] = [0; BINARY_VECTOR_SIZE];
        u64_list[..self.size_of_game_state].clone_from_slice(&input[..self.size_of_game_state]);
        let mut previous = BinaryVector::new(u64_list);
        let mut next: BinaryVector;
        for layer in 0..self.nn.len() {
            next = self.nn[layer].mul(&previous);
            std::mem::swap(&mut previous, &mut next);
        }
        previous.to_output(self.number_of_moves)
    }
}
